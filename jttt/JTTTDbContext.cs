﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace jttt
{
    class JTTTDbContext : DbContext
    {
        public JTTTDbContext()
            : base("JTTTAppDb")
        {
            Database.SetInitializer<JTTTDbContext>(new JTTTDbInitializer());
        }

        public DbSet<JTTTTrigger> Trigger { get; set; }
        public DbSet<JTTTAction>  Action { get; set; }
        public DbSet<JTTTRecipe>  Recipe { get; set; }
    }
}
