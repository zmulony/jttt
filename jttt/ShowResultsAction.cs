﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    [Serializable]
    class ShowResultsAction : JTTTAction
    {
        public string Subject 
        {
            get 
            {
                try
                {
                    return (string)InputData[0];
                }
                catch (InvalidCastException e)
                {
                    JTTTLogger.log(string.Format("FATAL ERROR: Error while casting input data for action. Reason: {0}.", e.Message.ToString()));
                }

                return null;
            }
        }

        public string Body
        {
            get 
            {
                try
                {
                    return (string)InputData[1];
                }
                catch (InvalidCastException e)
                {
                    JTTTLogger.log(string.Format("FATAL ERROR: Error while casting input data for action. Reason: {0}.", e.Message.ToString()));
                }

                return null;
            }
        }

        public MemoryStream Image
        {
            get
            {
                try
                {
                    return (MemoryStream)InputData[2];
                }
                catch (InvalidCastException e)
                {
                    JTTTLogger.log(string.Format("FATAL ERROR: Error while casting input data for action. Reason: {0}.", e.Message.ToString()));
                }

                return null;
            }
        }

        public ShowResultsAction() { }

        public override void Execute()
        {
            try
            {
                ResultsForm resultsForm = new ResultsForm();
                resultsForm.Subject = Subject;
                resultsForm.Body = Body;
                resultsForm.ImageStream = Image;
                resultsForm.Show();
                JTTTLogger.log("INFO: Showing ResultsForm.");
            }
            catch(Exception e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Error in ShowResultsAction. Reason: {0}.", e.Message.ToString()));
            }
        }

        public override string ToString()
        {
            return "";
        }
    }
}
