﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace jttt
{
    class JTTTSerializer
    {
        public void Serialize(object data, string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(fs, data);
            }
            catch (SerializationException e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Failed to serialize. Reason: {0}.", e.Message));
            }
            catch (ArgumentNullException e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Cannot serialize null data. Reason: {0}.", e.Message));
            }
            finally
            {
                fs.Close();
            }
        }

        public object Deserialize(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                return formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Failed to deserialize. Reason: {0}.", e.Message));
            }
            finally
            {
                fs.Close();
            }
            return null;
        }
    }
}
