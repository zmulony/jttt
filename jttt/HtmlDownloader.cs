﻿using System;
using System.Net;
using HtmlAgilityPack;
using ScrapySharp.Core;
using ScrapySharp.Html.Parsing;
using ScrapySharp.Extensions;
using System.Net.Mail;
using System.IO;

namespace jttt
{
    class HtmlDownloader
    {
        public HtmlAgilityPack.HtmlDocument downloadHtmlDocument(string htmlResUri)
        {
            try
            {
                WebClient client = new WebClient();
                JTTTLogger.log(string.Format("INFO: Preparing to download html from URI: {0}", htmlResUri));
                string data = client.DownloadString(htmlResUri);
                JTTTLogger.log(string.Format("INFO: Html downloaded. Size of downloaded string resource: {0} characters", data.Length));
                HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(data);

                return htmlDoc;
            }
            catch (Exception e)
            {
                JTTTLogger.log("FATAL ERROR: Error while trying to download html. Reason: " + e.Message.ToString());
            }

            return null;
        }
    }
}
