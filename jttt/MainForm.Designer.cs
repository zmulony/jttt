﻿namespace jttt
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.tcTrigger = new System.Windows.Forms.TabControl();
            this.tpFindText = new System.Windows.Forms.TabPage();
            this.tpTemperatureCheck = new System.Windows.Forms.TabPage();
            this.tcAction = new System.Windows.Forms.TabControl();
            this.tpSendMail = new System.Windows.Forms.TabPage();
            this.tpShowResults = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tcTrigger.SuspendLayout();
            this.tpFindText.SuspendLayout();
            this.tpTemperatureCheck.SuspendLayout();
            this.tcAction.SuspendLayout();
            this.tpSendMail.SuspendLayout();
            this.tpShowResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(373, 337);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(48, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(270, 20);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(47, 84);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(270, 20);
            this.textBox2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "URL";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tekst";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(50, 37);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(270, 20);
            this.textBox3.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Adres";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(177, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Jeżeli";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(194, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "to";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(152, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "na stronie";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(101, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(161, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "wyślij wiadomość na adres e-mail";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(373, 58);
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBox1.Size = new System.Drawing.Size(234, 277);
            this.listBox1.TabIndex = 11;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(292, 337);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "Dodaj";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(477, 337);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(130, 23);
            this.button3.TabIndex = 13;
            this.button3.Text = "Usuń zaznaczone";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(373, 366);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 14;
            this.button4.Text = "Serializuj";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(532, 366);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 15;
            this.button5.Text = "Deserializuj";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // tcTrigger
            // 
            this.tcTrigger.Controls.Add(this.tpFindText);
            this.tcTrigger.Controls.Add(this.tpTemperatureCheck);
            this.tcTrigger.Location = new System.Drawing.Point(12, 38);
            this.tcTrigger.Name = "tcTrigger";
            this.tcTrigger.SelectedIndex = 0;
            this.tcTrigger.Size = new System.Drawing.Size(355, 156);
            this.tcTrigger.TabIndex = 16;
            // 
            // tpFindText
            // 
            this.tpFindText.Controls.Add(this.label13);
            this.tpFindText.Controls.Add(this.textBox1);
            this.tpFindText.Controls.Add(this.textBox2);
            this.tpFindText.Controls.Add(this.label1);
            this.tpFindText.Controls.Add(this.label2);
            this.tpFindText.Controls.Add(this.label6);
            this.tpFindText.Location = new System.Drawing.Point(4, 22);
            this.tpFindText.Name = "tpFindText";
            this.tpFindText.Padding = new System.Windows.Forms.Padding(3);
            this.tpFindText.Size = new System.Drawing.Size(347, 130);
            this.tpFindText.TabIndex = 0;
            this.tpFindText.Text = "obrazek";
            this.tpFindText.UseVisualStyleBackColor = true;
            // 
            // tpTemperatureCheck
            // 
            this.tpTemperatureCheck.Controls.Add(this.label11);
            this.tpTemperatureCheck.Controls.Add(this.textBox5);
            this.tpTemperatureCheck.Controls.Add(this.label10);
            this.tpTemperatureCheck.Controls.Add(this.label9);
            this.tpTemperatureCheck.Controls.Add(this.textBox4);
            this.tpTemperatureCheck.Controls.Add(this.label8);
            this.tpTemperatureCheck.Location = new System.Drawing.Point(4, 22);
            this.tpTemperatureCheck.Name = "tpTemperatureCheck";
            this.tpTemperatureCheck.Padding = new System.Windows.Forms.Padding(3);
            this.tpTemperatureCheck.Size = new System.Drawing.Size(347, 130);
            this.tpTemperatureCheck.TabIndex = 1;
            this.tpTemperatureCheck.Text = "pogoda";
            this.tpTemperatureCheck.UseVisualStyleBackColor = true;
            // 
            // tcAction
            // 
            this.tcAction.Controls.Add(this.tpSendMail);
            this.tcAction.Controls.Add(this.tpShowResults);
            this.tcAction.Location = new System.Drawing.Point(14, 223);
            this.tcAction.Name = "tcAction";
            this.tcAction.SelectedIndex = 0;
            this.tcAction.Size = new System.Drawing.Size(353, 112);
            this.tcAction.TabIndex = 17;
            // 
            // tpSendMail
            // 
            this.tpSendMail.Controls.Add(this.textBox3);
            this.tpSendMail.Controls.Add(this.label3);
            this.tpSendMail.Controls.Add(this.label7);
            this.tpSendMail.Location = new System.Drawing.Point(4, 22);
            this.tpSendMail.Name = "tpSendMail";
            this.tpSendMail.Padding = new System.Windows.Forms.Padding(3);
            this.tpSendMail.Size = new System.Drawing.Size(345, 86);
            this.tpSendMail.TabIndex = 0;
            this.tpSendMail.Text = "mail";
            this.tpSendMail.UseVisualStyleBackColor = true;
            // 
            // tpShowResults
            // 
            this.tpShowResults.Controls.Add(this.label12);
            this.tpShowResults.Location = new System.Drawing.Point(4, 22);
            this.tpShowResults.Name = "tpShowResults";
            this.tpShowResults.Padding = new System.Windows.Forms.Padding(3);
            this.tpShowResults.Size = new System.Drawing.Size(345, 86);
            this.tpShowResults.TabIndex = 1;
            this.tpShowResults.Text = "wyświetl";
            this.tpShowResults.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(126, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "temperatura w mieście";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(90, 34);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(174, 20);
            this.textBox4.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(43, 37);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Miasto";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(142, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "jest wyższa niż";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(93, 83);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(171, 20);
            this.textBox5.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(17, 86);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Temperatura";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(88, 37);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(178, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "wyświetl wiadomość w nowym oknie";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(117, 68);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(147, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "znajduje się obrazek z opisem";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 402);
            this.Controls.Add(this.tcAction);
            this.Controls.Add(this.tcTrigger);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tcTrigger.ResumeLayout(false);
            this.tpFindText.ResumeLayout(false);
            this.tpFindText.PerformLayout();
            this.tpTemperatureCheck.ResumeLayout(false);
            this.tpTemperatureCheck.PerformLayout();
            this.tcAction.ResumeLayout(false);
            this.tpSendMail.ResumeLayout(false);
            this.tpSendMail.PerformLayout();
            this.tpShowResults.ResumeLayout(false);
            this.tpShowResults.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TabControl tcTrigger;
        private System.Windows.Forms.TabPage tpFindText;
        private System.Windows.Forms.TabPage tpTemperatureCheck;
        private System.Windows.Forms.TabControl tcAction;
        private System.Windows.Forms.TabPage tpSendMail;
        private System.Windows.Forms.TabPage tpShowResults;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
    }
}

