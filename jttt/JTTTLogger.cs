﻿using System;
using System.IO;

namespace jttt
{
    class JTTTLogger
    {
        public static void log(String message)
        {
            DateTime datetime = DateTime.Now;
            String oFileName = "jttt.log";

            if (!File.Exists(oFileName))
            {
                FileStream f = File.Create(oFileName);
                f.Close();
            }

            try
            {
                StreamWriter writter = File.AppendText(oFileName);
                writter.WriteLine(datetime.ToString("[MM/dd/yyyy H:mm:ss]") + " >> " + message);
                writter.Flush();
                writter.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
            }
        }
    }
}
