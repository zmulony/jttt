﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    [Serializable]
    class TemperatureCheckTrigger : JTTTTrigger
    {
        public string City { get; set; }
        public double MinTemp { get; set; }

        private double _actualTemp;
        private MemoryStream _imageStream;

        private TemperatureCheckTrigger() { }

        public TemperatureCheckTrigger(string city, double minTemp)
        {
            City = city;
            MinTemp = minTemp;
        }

        public override bool Trigger()
        {
            try
            {
                WebClient webClient = new WebClient();

                StringBuilder builder = new StringBuilder("http://api.openweathermap.org/data/2.5/weather?");
                builder.Append(City != null ? "q=" + City : "");
                var json = webClient.DownloadString(builder.ToString());

                builder.Clear();

                var weather = JsonConvert.DeserializeObject<RootObject>(json);
                _actualTemp = weather.Temp;
                if (_actualTemp >= MinTemp)
                {
                    HasBeenTriggered = true;
                    builder.Append("http://api.openweathermap.org/img/w/");
                    builder.Append(weather.weather[0].icon);
                    builder.Append(".png");
                    _imageStream = new MemoryStream(webClient.DownloadData(builder.ToString()));
                    JTTTLogger.log(string.Format("INFO: Image downloaded. Size: {0} bytes", _imageStream.Length));
                }
                else
                {
                    HasBeenTriggered = false;
                    JTTTLogger.log("INFO: Image not found.");
                }
            }
            catch (Exception e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Error in TemperatureCheckTrigger. Reason: {0}.", e.Message.ToString()));
            }
            PrepareOutputData();
            return HasBeenTriggered;
        }

        private void PrepareOutputData()
        {
            OutputData = new object[3];
            OutputData[0] = string.Format("JTTT Weather checker", City);
            OutputData[1] = string.Format("{0}: Aktualna temperatura wynosi {1}C.", City, _actualTemp.ToString("f1"));
            OutputData[2] = _imageStream;
        }

        public override string ToString()
        {
            return string.Format("City={0} MinTemp={1}", City, MinTemp);
        }
    }
}
