﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace jttt
{
    [Serializable]
    abstract class JTTTTrigger
    {
        [Key]
        public int Id { get; set; }

        public bool HasBeenTriggered { get; protected set; }

        public virtual object[] OutputData { get; protected set; }

        public abstract bool Trigger();
    }
}
