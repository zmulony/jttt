﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jttt
{
    class JTTTDbInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<JTTTDbContext>
    {
        protected override void Seed(JTTTDbContext ctx)
        {
            ctx.SaveChanges();
            base.Seed(ctx);
        }
    }
}
