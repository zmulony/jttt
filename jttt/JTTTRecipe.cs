﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace jttt
{
    [Serializable]
    class JTTTRecipe 
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public virtual JTTTTrigger Trigger { get; set; }
        [Required]
        public virtual JTTTAction Action { get; set; }

        private JTTTRecipe() { }

        public JTTTRecipe(JTTTTrigger trigger, JTTTAction action)
        {
            Trigger = trigger;
            Action  = action;
        }

        public void Run()
        {
            if (Trigger.Trigger())
            {
                Action.InputData = Trigger.OutputData;
                Action.Execute();
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", Trigger.ToString(), Action.ToString());
        }
    }
}
