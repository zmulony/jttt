﻿using System;
using System.Net;
using System.Net.Mail;
using System.IO;

namespace jttt
{
    class Mailer
    {
        public NetworkCredential Credentials { get; set; }
        public int Port { get; set; }
        public string Host { get; set; }

        public Mailer(string  host, int port, NetworkCredential credentials)
        {
            Host = host;
            Port = port;
            Credentials = credentials;
        }

        public void Send(MailMessage mailMsg)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient(Host, Port);
                JTTTLogger.log(string.Format("INFO: SmtpClient: host {0} port {1}.", smtpClient.Host, smtpClient.Port));
                smtpClient.EnableSsl = true;
                if (Credentials != null)
                {
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = Credentials;
                }

                smtpClient.Send(mailMsg);
                JTTTLogger.log("INFO: Mail has been sent.");
            }
            catch (Exception e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Error while trying to send message. Reason: {0}.", e.Message.ToString ()));
            }
        }
    }
}
