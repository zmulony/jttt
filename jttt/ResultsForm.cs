﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace jttt
{
    public partial class ResultsForm : Form
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public MemoryStream ImageStream { get; set; }

        public ResultsForm()
        {
            InitializeComponent();
        }

        private void ResultsForm_Load(object sender, EventArgs e)
        {
            richTextBox1.Text = Subject;
            richTextBox2.Text = Body;
            pictureBox1.Image = Image.FromStream(ImageStream);
        }
    }
}
