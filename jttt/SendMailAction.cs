﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using HtmlAgilityPack;
using ScrapySharp.Core;
using ScrapySharp.Html.Parsing;
using ScrapySharp.Extensions;
using System.Net.Mail;
using System.IO;
using System.Net.Mime;

namespace jttt
{
    [Serializable]
    class SendMailAction : JTTTAction
    {
        public string MailAddress { get; set; }

        public string MailMsgSubject 
        {
            get 
            {
                try
                {
                    return (string)InputData[0];
                }
                catch (InvalidCastException e)
                {
                    JTTTLogger.log(string.Format("FATAL ERROR: Error while casting input data for action. Reason: {0}.", e.Message.ToString()));
                }

                return null;
            }
        }

        public string MailMsgBody
        {
            get 
            {
                try
                {
                    return (string)InputData[1];
                }
                catch (InvalidCastException e)
                {
                    JTTTLogger.log(string.Format("FATAL ERROR: Error while casting input data for action. Reason: {0}.", e.Message.ToString()));
                }

                return null;
            }
        }

        public MemoryStream ImageAttachment
        {
            get
            {
                try
                {
                    return (MemoryStream)InputData[2];
                }
                catch (InvalidCastException e)
                {
                    JTTTLogger.log(string.Format("FATAL ERROR: Error while casting input data for action. Reason: {0}.", e.Message.ToString()));
                }

                return null;
            }
        }

        private SendMailAction() { }

        public SendMailAction(string mailAddress)
        {
            MailAddress = mailAddress;
        }

        public override void Execute()
        {
            try
            {
                // prepare mail message
                MailMessage mailMsg = new MailMessage("jtttapp@gmail.com", MailAddress, MailMsgSubject, MailMsgBody);
                mailMsg.Attachments.Add(new Attachment(ImageAttachment, "JTTT.jpg"));
                JTTTLogger.log(string.Format("INFO: Mail attachments count: {0}.", mailMsg.Attachments.Count));

                // send mail
                Mailer mailer = new Mailer("smtp.gmail.com", 587, new NetworkCredential("jtttapp@gmail.com", "applicationPassword"));
                mailer.Send(mailMsg);
            }
            catch(Exception e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Error in SendMailAction. Reason: {0}.", e.Message.ToString()));
            }
        }

        public override string ToString()
        {
            return string.Format("MailAddress={0}", MailAddress);
        }
    }
}
