﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace jttt
{
    [Serializable]
    abstract class JTTTAction
    {
        [Key]
        public int Id { get; set; }

        public virtual object[] InputData { get; set; }

        public abstract void Execute();
    }
}
