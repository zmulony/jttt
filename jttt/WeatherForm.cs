﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using Newtonsoft.Json;
using System.IO;


namespace jttt
{
    public partial class WeatherForm : Form
    {
        public WeatherForm()
        {
            InitializeComponent();
        }

        private void WeatherForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            WebClient webClient = new WebClient();

            StringBuilder builder = new StringBuilder("http://api.openweathermap.org/data/2.5/weather?");
            builder.Append(textBox1.Text != null ? "q=" + textBox1.Text : "");
            var json = webClient.DownloadString(builder.ToString());

            builder.Clear();

            var weather = JsonConvert.DeserializeObject<RootObject>(json);
            builder.AppendLine(string.Format("Dzisiejsza temperatura: {0} C.", weather.Temp.ToString("f1")));
            builder.AppendLine(string.Format("Ciśnienie: {0} hPa.", weather.main.pressure));
            builder.AppendLine(string.Format("Niebo: {0}.", weather.weather[0].description));
            richTextBox1.Text = builder.ToString();

            builder.Clear();

            builder.Append("http://api.openweathermap.org/img/w/");
            builder.Append(weather.weather[0].icon);
            builder.Append(".png");
            MemoryStream stream = new MemoryStream(webClient.DownloadData(builder.ToString()));
            pictureBox1.Image = Image.FromStream(stream);
        }
    }

    public class Coord
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }

    public class Sys
    {
        public double message { get; set; }
        public string country { get; set; }
        public int sunrise { get; set; }
        public int sunset { get; set; }
    }

    public class Weather
    {
        public int id { get; set; }
        public string main { get; set; }
        public string description { get; set; }
        public string icon { get; set; }
    }

    public class Main
    {
        public double temp { get; set; }
        public int humidity { get; set; }
        public int pressure { get; set; }
        public double temp_min { get; set; }
        public double temp_max { get; set; }
    }

    public class Wind
    {
        public double speed { get; set; }
        public double gust { get; set; }
        public int deg { get; set; }
    }

    public class Rain
    {
        public int __invalid_name__3h { get; set; }
    }

    public class Clouds
    {
        public int all { get; set; }
    }

    public class RootObject
    {
        public Coord coord { get; set; }
        public Sys sys { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public Wind wind { get; set; }
        public Rain rain { get; set; }
        public Clouds clouds { get; set; }
        public int dt { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int cod { get; set; }

        public double Temp
        {
            get { return (main.temp - 273.0); }
        }
    }
}
