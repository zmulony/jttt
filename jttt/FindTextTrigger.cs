﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using ScrapySharp.Core;
using ScrapySharp.Html.Parsing;
using ScrapySharp.Extensions;
using System.IO;

namespace jttt
{
    [Serializable]
    class FindTextTrigger : JTTTTrigger
    {
        public string HtmlResourceUri { get; set; }
        public string TextToFind { get; set; }

        private string _imageUri;
        private MemoryStream _imageStream;

        private FindTextTrigger() { }

        public FindTextTrigger(string htmlResourceUri, string textToFind)
        {
            HtmlResourceUri = htmlResourceUri;
            TextToFind = textToFind;
        }

        public override bool Trigger()
        {
            try
            {
                // download html from url
                HtmlDownloader htmlDownloader = new HtmlDownloader();
                HtmlAgilityPack.HtmlDocument htmlDoc = htmlDownloader.downloadHtmlDocument(HtmlResourceUri);

                // select proper images in <img></img> tags
                WebClient client = new WebClient();
                string xpath = "//img[contains(@src, 'http://') and (@title or @alt)]";
                var node = htmlDoc.DocumentNode.SelectNodes(xpath)
                    .Where((n) => n.GetAttributeValue("title").ToLower().Contains(TextToFind.ToLower()) ||
                                  n.GetAttributeValue("alt").ToLower().Contains(TextToFind.ToLower()))
                    .First();
                if (node != null)
                {
                    HasBeenTriggered = true;
                    _imageUri = node.GetAttributeValue("src");
                    JTTTLogger.log(string.Format("INFO: Found image. title: {0}, alt: {1}. Image source: {2}.", node.GetAttributeValue("title"), node.GetAttributeValue("alt"), _imageUri));
                    _imageStream = new MemoryStream(client.DownloadData(_imageUri));
                    JTTTLogger.log(string.Format("INFO: Image downloaded. Size: {0} bytes",_imageStream.Length));
                }
                else
                {
                    HasBeenTriggered = false;
                    JTTTLogger.log("INFO: Image not found.");
                }
            }
            catch (Exception e)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Error in FindTextTrigger. Reason: {0}.", e.Message.ToString()));
            }
            PrepareOutputData();
            return HasBeenTriggered;
        }

        private void PrepareOutputData()
        {
            OutputData = new object[3];
            OutputData[0] = "JTTT Picture finder";
            OutputData[1] = string.Format("Na stronie {0} znajduje się obrazek związany z hasłem '{1}'.", HtmlResourceUri, TextToFind);
            OutputData[2] = _imageStream;
        }

        public override string ToString()
        {
            return string.Format("HtmlResourceUri={0} TextToFind={1}", HtmlResourceUri, TextToFind);
        }
    }
}
