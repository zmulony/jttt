﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace jttt
{
    public partial class MainForm : Form
    {
        BindingList<JTTTRecipe> tasksList;
        JTTTSerializer serializer;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            serializer = new JTTTSerializer();
            tasksList = new BindingList<JTTTRecipe>();
            listBox1.DataSource = tasksList;

            using (var ctx = new JTTTDbContext())
            {
                var recipes = ctx.Recipe.Include("Trigger").Include("Action");
                foreach (var r in recipes)
                {
                    tasksList.Add(r);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            JTTTLogger.log("INFO: Start button has been pressed");

            try
            {
                foreach (var task in tasksList)
                {
                    task.Run();
                }
            }
            catch(Exception ex)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Error while running recipe. Reason: {0}", ex.Message.ToString()));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            JTTTLogger.log("INFO: Dodaj button has been pressed");

            try
            {
                JTTTTrigger trigger;
                if (tcTrigger.SelectedTab.Text == tpFindText.Text)
                {
                    trigger = new FindTextTrigger(textBox1.Text, textBox2.Text);
                }
                else
                {
                    trigger = new TemperatureCheckTrigger(textBox4.Text, double.Parse(textBox5.Text));
                }

                JTTTAction action;
                if (tcAction.SelectedTab.Text == tpSendMail.Text)
                {
                    action = new SendMailAction(textBox3.Text);
                }
                else
                {
                    action = new ShowResultsAction();
                }

                JTTTRecipe recipe = new JTTTRecipe(trigger, action);
                JTTTLogger.log(string.Format("INFO: New recipe has been created. Trigger: {0}. Action: {1}.", trigger.GetType(), action.GetType()));
                tasksList.Add(recipe);

                using (var ctx = new JTTTDbContext())
                {
                    ctx.Recipe.Add(recipe);
                    JTTTLogger.log(string.Format("INFO: Added recipe: {0}.", recipe));
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                JTTTLogger.log(string.Format("FATAL ERROR: Reason: {0}", ex.Message.ToString()));
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            JTTTLogger.log("INFO: Usun zaznaczone button has been pressed");
            using (var ctx = new JTTTDbContext())
            {
                foreach (JTTTRecipe t in listBox1.SelectedItems)
                {
                    JTTTRecipe recipe = ctx.Recipe.Find(t.Id);
                    if (recipe != null)
                    {
                        ctx.Recipe.Remove(recipe);
                        JTTTLogger.log(string.Format("INFO: Removed recipe: {0}.", t));
                    }
                    else
                    {
                        JTTTLogger.log(string.Format("INFO: Remove aborted. Recipe {0} could not be found.", t));
                    }
                }
                ctx.SaveChanges();

                tasksList.Clear();
                var recipes = ctx.Recipe.Include("Trigger").Include("Action");
                foreach (var r in recipes)
                {
                    tasksList.Add(r);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            JTTTLogger.log("INFO: Serializuj button has been pressed");
            serializer.Serialize(tasksList, "JTTTDataFile.dat");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            JTTTLogger.log("INFO: Deserializuj button has been pressed");
            using (var ctx = new JTTTDbContext())
            {
                foreach (var t in tasksList)
                {
                    JTTTAction action = ctx.Action.Find(t.Id);
                    if (action != null)
                    {
                        ctx.Action.Remove(action);
                        JTTTLogger.log(string.Format("INFO: Removed recipe: {0}.", t));
                    }
                    else
                    {
                        JTTTLogger.log(string.Format("INFO: Remove aborted. Recipe {0} could not be found.", t));
                    }
                }

                tasksList = (BindingList<JTTTRecipe>)serializer.Deserialize("JTTTDataFile.dat");
                foreach (var t in tasksList)
                {
                    ctx.Recipe.Add(t);
                    JTTTLogger.log(string.Format("INFO: Added recipe: {0}.", t));
                }
                ctx.SaveChanges();
                listBox1.DataSource = tasksList;
            }
        }
    }
}
